drop database if exists pollokahvila;
create database pollokahvila;
use pollokahvila;

create table asiakas (
  asnro int not null primary key,
  nimi varchar(100) not null,
  sposti varchar(255) not null,
  puhelin varchar(20) not null,
  postinumero char(5)
);

create table varaus (
    varausnro int not null primary key,
    varauspvm timestamp not null,
    asnro int not null,
    foreign key (asnro) references asiakas(asnro)
    on delete restrict
);

create table tyontekija (
    tt_numero int primary key,
    nimi varchar (100)
);


create table tyovuoro (
    id int primary key auto_increment,
    alku datetime not null,
    loppu datetime not null,
    tt_numero int not null,
    foreign key (tt_numero) references tyontekija(tt_numero)
    on delete restrict
);

create table tilatyyppi (
    id int primary key auto_increment,
    nimi varchar (50) not null,
    varauskesto int not null,
    maks_maara smallint,
    tyovuoro_id int not NULL,
    foreign key (tyovuoro_id) references tyovuoro(id)
    on delete restrict
);

create table tila (
    tilanro int primary key not null,
    nimi smallint not null,
    tilatyyppi_id int not null,
    foreign key (tilatyyppi_id) references tilatyyppi(id)
    on delete restrict
);

create table varausrivi (
    varausnro int not null,
    tila int not null,
    ajankohta datetime not null,
    henk_maara smallint not null,
    foreign key (varausnro) references varaus(varausnro)
    on delete restrict,
    foreign key (tila) references tila(tilanro)
    on delete restrict
);




